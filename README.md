
# Einstein Toolit Jenkins Container

To start a container suitable for acting as a Jenkins worker, on your
Docker host, run

    docker run --restart=always -d --name etworker -p 2023:22 einsteintoolkit/et-jenkins-worker:ubuntu-20.04

You can name it whatever you like instead of etworker, and adapt the
ssh source port 2023 to whatever port you want to listen on for ssh
connections from the Jenkins master.

Contact the ET Jenkins administrator to have your worker added to the
Jenkins system.  You will need to provide the host and port for ssh
connections.

You can choose another operating system by specifying a different tag, i.e.

    einsteintoolkit/et-jenkins-worker:<tag>

The available tags are listed at https://hub.docker.com/r/einsteintoolkit/et-jenkins-worker/tags/.

You can add your own ssh key to the container using

    docker exec -i etworker sh -c "cat >>/home/jenkins/.ssh/authorized_keys" < ~/.ssh/id_rsa.pub

and then log in as usual via ssh:

    ssh -p 2023 jenkins@localhost

# Cleaning up

You can stop the container with

    docker stop etworker

and restart it with

    docker start etworker

You can remove the container, including any changes you have made to
its filesystem, with

    docker rm etworker
